import { BaseException } from './base.exception';
import { ExceptionCodes } from './exception.codes';

export class NotFoundException extends BaseException {
  constructor(message = 'Not found') {
    super(message);
  }
  readonly code = ExceptionCodes.notFound;
}
