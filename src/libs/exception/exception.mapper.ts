import { BaseException } from './base.exception';
import { NotFoundException } from './not-found.exception';
import {
  HttpException,
  // To avoid confusion between internal app exceptions and NestJS exceptions
  NotFoundException as NestNotFoundException,
  InternalServerErrorException as NestInternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { ExceptionCodes } from './exception.codes';
import { CodeUtils } from './code.utils';

export interface ErrorResponseBody {
  message: string;
  code: string;
}

export class ExceptionMapper {
  private readonly logger = new Logger(ExceptionMapper.name);
  public toHttpException(exception: BaseException): HttpException {
    const errorResponseBody: ErrorResponseBody = this.getBody(exception);
    switch (exception.constructor) {
      case NotFoundException:
        return new NestNotFoundException(errorResponseBody);
    }
    this.logger.warn('Exception with missing mapping detected');
    return new NestInternalServerErrorException(errorResponseBody);
  }

  public standardizeException(exception: HttpException): HttpException {
    const response: any = exception.getResponse();
    const error: ErrorResponseBody = {
      message: response.message ? response.message : exception.message,
      code: CodeUtils.getCodeFromNestException(exception),
    };
    return new (<any>exception.constructor)(error);
  }

  public toInternalServerException(error): HttpException {
    return new NestInternalServerErrorException(this.getBody(error));
  }

  private getBody(error): ErrorResponseBody {
    const code = error.code ? error.code : ExceptionCodes.internalServerError;
    return { message: error.message, code: code };
  }
}
