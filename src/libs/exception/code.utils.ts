import { ExceptionCodes } from './exception.codes';
import {
  HttpException,
  // To avoid confusion between internal app exceptions and NestJS exceptions
  BadRequestException as NestBadRequestException,
  NotFoundException as NestNotFoundException,
} from '@nestjs/common';

export class CodeUtils {
  public static getCodeFromNestException(
    exception: HttpException,
  ): ExceptionCodes {
    switch (exception.constructor) {
      case NestNotFoundException:
        return ExceptionCodes.notFound;
      case NestBadRequestException:
        return ExceptionCodes.badRequest;
    }
    return ExceptionCodes.internalServerError;
  }
}
