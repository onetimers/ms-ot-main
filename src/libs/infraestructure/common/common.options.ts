import { config } from 'dotenv';
import { get } from 'env-var';

// Initializing dotenv
config();

class CommonConfig {
  public static readonly NODE_ENV: string = get('NODE_ENV')
    .required()
    .asString();
}

export const commonOptions = {
  isProd: CommonConfig.NODE_ENV == 'production',
};
