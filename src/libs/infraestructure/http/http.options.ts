import { Request } from 'express';
import { TRACE_HEADER } from './trace/constants';
import { REQUEST } from '@nestjs/core';
import { HttpModuleAsyncOptions } from '@nestjs/axios';
import { config } from 'dotenv';
import { get } from 'env-var';

// Initializing dotenv
config();

class HttpConfig {
  public static readonly HTTP_TIMEOUT: number = get('HTTP_TIMEOUT')
    .required()
    .asIntPositive();
}

export const httpOptions: HttpModuleAsyncOptions = {
  useFactory: async (req: Request) => ({
    timeout: HttpConfig.HTTP_TIMEOUT,
    headers: { [TRACE_HEADER]: req.locals.traceId },
  }),
  inject: [REQUEST],
};
