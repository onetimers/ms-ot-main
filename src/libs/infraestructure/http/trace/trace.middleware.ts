import { Injectable, Logger, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { customAlphabet } from 'nanoid/non-secure';
import { TRACE_HEADER } from './constants';

@Injectable()
export class TraceMiddleware implements NestMiddleware {
  private readonly generator = customAlphabet(
    '1234567890abcdefghijklmnopqrstuvwxyz',
    16,
  );

  private readonly logger = new Logger(TraceMiddleware.name);

  use(req: Request, res: Response, next: NextFunction) {
    let traceId: string = req.header(TRACE_HEADER);
    if (!traceId) {
      this.logger.debug(`Header ${TRACE_HEADER} missing from request.`);
      traceId = this.generator();
    }
    const spanId: string = this.generator();
    req.locals = { traceId: traceId, spanId: spanId };
    res.setHeader(TRACE_HEADER, traceId);
    next();
  }
}
