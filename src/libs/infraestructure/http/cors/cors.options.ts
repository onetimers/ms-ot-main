import { CorsOptions } from '@nestjs/common/interfaces/external/cors-options.interface';
import { commonOptions } from '../../common/common.options';

export const corsOptions: CorsOptions = {
  origin: commonOptions.isProd ? /mgames\.com/ : '*',
  methods: 'OPTIONS,GET,HEAD,PUT,PATCH,POST,DELETE',
};
