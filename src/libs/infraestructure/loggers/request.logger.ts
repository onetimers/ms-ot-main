export const requestLogger = (req, res) => {
  return `Request received: ${buildPath(req)} ${buildHeaders(req)} ${buildTrace(req)}`;
};

const buildPath = (req) => {
  return `${req.method} ${req.originalUrl}`;
};

const buildHeaders = (req) => {
  return `with headers ${JSON.stringify(req.headers)}`;
};

const buildTrace = (req) => {
  return `from remote ${JSON.stringify(req.socket._peername)}`;
};
