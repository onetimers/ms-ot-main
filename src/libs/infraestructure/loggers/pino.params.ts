import { Params } from 'nestjs-pino';
import { DateUtils } from '../../utils/date.utils';
import { requestLogger } from './request.logger';
import { responseLogger } from './response.logger';
import { commonOptions } from '../common/common.options';

//https://github.com/pinojs/pino-pretty/issues/54
const pretty = commonOptions.isProd
  ? undefined
  : {
      target: 'pino-pretty',
      options: {
        ignore: 'pid,hostname',
        colorize: true,
        levelFirst: true,
        singleLine: true,
      },
    };

export const pinoParams: Params = {
  pinoHttp: [
    {
      useLevel: 'debug',
      timestamp: () => `,"time":"${DateUtils.now()}"`,
      redact: {
        paths: ['req', 'res', 'err'],
        remove: true,
      },
      autoLogging: !commonOptions.isProd,
      level: commonOptions.isProd ? 'info' : 'debug',
      genReqId: () => undefined,
      reqCustomProps: (req_, res_) => {
        const req: any = req_;
        return { tracing: req.locals };
      },
      serializers: {
        req(req) {
          req.locals = req.raw.locals;
          return req;
        },
      },
      customReceivedMessage: (req, res) => requestLogger(req, res),
      customSuccessMessage: (res) => responseLogger(res),
      customErrorMessage: (err, res) => responseLogger(res),
      transport: pretty,
    },
    ,
  ],
};
