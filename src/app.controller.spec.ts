import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoggerModule } from 'nestjs-pino';
import { pinoParams } from './libs/infraestructure/loggers/pino.params';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeormOptions } from './libs/infraestructure/orm/orm.options';
import { HttpModule, HttpService } from '@nestjs/axios';
import { httpOptions } from './libs/infraestructure/http/http.options';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [
        AppService,
        {
          provide: HttpService,
          useValue: {
            post: jest.fn(),
          },
        },
      ],
    }).compile();
    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(appController.getHello()).toBe('Hello World!');
    });
  });
});
