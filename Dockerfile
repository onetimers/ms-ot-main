FROM gitlab.com:443/mgames1/dependency_proxy/containers/node@sha256:38bc06c682ae1f89f4c06a5f40f7a07ae438ca437a2a04cf773e66960b2d75bc

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

COPY . .

EXPOSE 8080

ENTRYPOINT ["node","/usr/src/app/dist/main"]
